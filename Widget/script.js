var facetasResultados = [];

// Cria a estrutura do <body>
function criarBody(widget) {
    var main = document.getElementById(widget);
    pesquisarSensoresAJAX();
    var form = document.createElement("form");
    var table = document.createElement("table");
    table.setAttribute("id", "mainTable");
    var row = document.createElement("tr");
    row.setAttribute("id", "mainRow");
    table.appendChild(row);
    form.setAttribute("id", "widgetForm");
    form.appendChild(table);
    main.appendChild(form);
}

// Definição da função do padrão dado nas aulas TPs
function makeXmlHttpGetCall(url, params, async, xml, callback, args) {
    var objHTTP = new XMLHttpRequest();
    if (objHTTP) {
        objHTTP.open("GET", url, async);
        objHTTP.onreadystatechange = function () {
            if (objHTTP.readyState == 4 && objHTTP.status == 200) {
                var response;
                if (xml == true) {
                    response = objHTTP.responseXML;

                } else {
                    response = objHTTP.responseText;
                }
                callback(response, args);
            }
        };
        objHTTP.send(params);
    }
}

// Faz o pedido dos sensores em AJAX à API
function pesquisarSensoresAJAX() {
    url = "http://phpdev2.dei.isep.ipp.pt/~nsilva/smartcity/sensores.php";
    makeXmlHttpGetCall(url, null, true, true, pesquisarSensoresXML, null);
}

// Faz o pedido dos sensores em XML da resposta da API
function pesquisarSensoresXML(objXML, args) {
    var sensoresXML = objXML.getElementsByTagName("sensor");
    var sensores = [];
    for (var i = 0; i < sensoresXML.length; i++) {
        var nodeNomeSensor = sensoresXML[i].childNodes[0];
        sensores[i] = nodeNomeSensor.textContent;
    }
    criarTabsSensores(sensores);

}

// Cria os elementos HTML através do DOM
function criarTabsSensores(sensores) {
    var menuTabs = document.createElement("ul");
    var sensoresColuna = document.createElement("td");
    sensoresColuna.setAttribute("id", "colSensores");
    menuTabs.setAttribute("class", "menuTabs");

    for (var i = 0; i < sensores.length; i++) {
        var tab = document.createElement("li");
        var link = document.createElement("a");
        link.setAttribute("href", "#");
        switch (i) {
            case 0:
                link.setAttribute("onclick", "pesquisarFacetasTemperaturaAJAX()");
                break;
            case 1:
                link.setAttribute("onclick", "pesquisarFacetasQualidadeArAJAX()");
                break;
            case 2:
                link.setAttribute("onclick", "pesquisarFacetasFluxoTransitoAJAX()");
                break;
            case 3:
                link.setAttribute("onclick", "pesquisarFacetasAtividadeCardiacaAJAX()");
                break;
        }
        link.appendChild(document.createTextNode(sensores[i]));
        tab.appendChild(link);
        menuTabs.appendChild(tab);
    }

    sensoresColuna.appendChild(menuTabs);
    var row = document.getElementById("mainRow");
    row.appendChild(sensoresColuna);

}

// Faz o pedido das facetas da Temperatura (sensor = 1) em AJAX à API
function pesquisarFacetasTemperaturaAJAX() {
    url = "http://phpdev2.dei.isep.ipp.pt/~nsilva/smartcity/facetas.php?sensor=1";
    makeXmlHttpGetCall(url, null, true, true, pesquisarFacetasTemperaturaXML, null);
}

// Faz o pedido das facetas da Temperatura (sensor = 1) em XML da resposta da API
function pesquisarFacetasTemperaturaXML(objXML, args) {
    var facetasXML = objXML.getElementsByTagName("Faceta");
    var facetasTipo = [];
    var facetas = [];
    for (var i = 0; i < facetasXML.length; i++) {
        var nodeFaceta = facetasXML[i].childNodes[0];
        facetas[i] = nodeFaceta.textContent;
        var nodeTipo = facetasXML[i].childNodes[3];
        facetasTipo[i] = nodeTipo.textContent;
    }
    criarPainelTemperatura(facetas, facetasTipo);
}


// Cria o painel das facetas da Temperatura (sensor = 1)
function criarPainelTemperatura(facetas, facetasTipo) {
    var row = document.getElementById("mainRow");
    if (!document.getElementById("painelFacetas")) {
        var divPainel = document.createElement("div");
        divPainel.setAttribute("id", "painelFacetas");
        preencheCheckboxsFacetas(facetas, divPainel, facetasTipo, "Temperatura");
        row.appendChild(divPainel);
        pesquisaResultadosAJAX("Temperatura", facetas);
    } else {
        row.childNodes[1].textContent = "";
        var divPainel = document.getElementById("painelFacetas");
        preencheCheckboxsFacetas(facetas, divPainel, facetasTipo, "Temperatura");
        row.appendChild(divPainel);
        pesquisaResultadosAJAX("Temperatura", facetas);
    }
}

// Faz o pedido das facetas da Qualidade do Ar (sensor = 2) em AJAX à API
function pesquisarFacetasQualidadeArAJAX() {
    url = "http://phpdev2.dei.isep.ipp.pt/~nsilva/smartcity/facetas.php?sensor=2";
    makeXmlHttpGetCall(url, null, true, true, pesquisarFacetasQualidadeArXML, null);
}

// Faz o pedido das facetas da Qualidade do Ar (sensor = 2) em XML da resposta da API
function pesquisarFacetasQualidadeArXML(objXML, args) {
    var facetasXML = objXML.getElementsByTagName("Faceta");
    var facetasTipo = [];
    var facetas = [];
    for (var i = 0; i < facetasXML.length; i++) {
        var nodeFaceta = facetasXML[i].childNodes[0];
        facetas[i] = nodeFaceta.textContent;
        var nodeTipo = facetasXML[i].childNodes[3];
        facetasTipo[i] = nodeTipo.textContent;
    }
    criarPainelQualidadeAr(facetas, facetasTipo);
}

// Cria o painel das facetas da Qualidade do Ar (sensor = 2)
function criarPainelQualidadeAr(facetas, facetasTipo) {
    var row = document.getElementById("mainRow");
    if (!document.getElementById("painelFacetas")) {
        var divPainel = document.createElement("div");
        divPainel.setAttribute("id", "painelFacetas");
        preencheCheckboxsFacetas(facetas, divPainel, facetasTipo, "Qualidade_do_ar");
        row.appendChild(divPainel);
        pesquisaResultadosAJAX("Qualidade_do_ar", facetas);
    } else {
        row.childNodes[1].textContent = "";
        var divPainel = document.getElementById("painelFacetas");
        preencheCheckboxsFacetas(facetas, divPainel, facetasTipo, "Qualidade_do_ar");
        row.appendChild(divPainel);
        pesquisaResultadosAJAX("Qualidade_do_ar", facetas);
    }
}

// Faz o pedido das facetas do Fluxo de Trânsito (sensor = 3) em AJAX à API
function pesquisarFacetasFluxoTransitoAJAX() {
    url = "http://phpdev2.dei.isep.ipp.pt/~nsilva/smartcity/facetas.php?sensor=3";
    makeXmlHttpGetCall(url, null, true, true, pesquisarFacetasFluxoTransitoXML, null);
}

// Faz o pedido das facetas do Fluxo de Trânsito (sensor = 3) em XML da resposta da API
function pesquisarFacetasFluxoTransitoXML(objXML, args) {
    var facetasXML = objXML.getElementsByTagName("Faceta");
    var facetasTipo = [];
    var facetas = [];
    for (var i = 0; i < facetasXML.length; i++) {
        var nodeFaceta = facetasXML[i].childNodes[0];
        facetas[i] = nodeFaceta.textContent;
        var nodeTipo = facetasXML[i].childNodes[3];
        facetasTipo[i] = nodeTipo.textContent;
    }
    criarPainelFluxoTransito(facetas, facetasTipo);
}

// Cria o painel das facetas do Fluxo de Trânsito (sensor = 3)
function criarPainelFluxoTransito(facetas, facetasTipo) {
    var row = document.getElementById("mainRow");
    if (!document.getElementById("painelFacetas")) {
        var divPainel = document.createElement("div");
        divPainel.setAttribute("id", "painelFacetas");
        preencheCheckboxsFacetas(facetas, divPainel, facetasTipo, "Fluxo_de_transito");
        row.appendChild(divPainel);
        pesquisaResultadosAJAX("Fluxo_de_transito", facetas);
    } else {
        row.childNodes[1].textContent = "";
        var divPainel = document.getElementById("painelFacetas");
        preencheCheckboxsFacetas(facetas, divPainel, facetasTipo, "Fluxo_de_transito");
        row.appendChild(divPainel);
        pesquisaResultadosAJAX("Fluxo_de_transito", facetas);
    }
}

// Faz o pedido das facetas da Atividade Cardíaca (sensor = 4) em AJAX à API
function pesquisarFacetasAtividadeCardiacaAJAX() {
    url = "http://phpdev2.dei.isep.ipp.pt/~nsilva/smartcity/facetas.php?sensor=4";
    makeXmlHttpGetCall(url, null, true, true, pesquisarFacetasAtividadeCardiacaXML, null);
}

// Faz o pedido das facetas da Atividade Cardíaca (sensor = 4) em XML da resposta da API
function pesquisarFacetasAtividadeCardiacaXML(objXML, args) {
    var facetasXML = objXML.getElementsByTagName("Faceta");
    var facetasTipo = [];
    var facetas = [];
    for (var i = 0; i < facetasXML.length; i++) {
        var nodeFaceta = facetasXML[i].childNodes[0];
        facetas[i] = nodeFaceta.textContent;
        var nodeTipo = facetasXML[i].childNodes[3];
        facetasTipo[i] = nodeTipo.textContent;
    }
    criarPainelAtividadeCardiaca(facetas, facetasTipo);
}

// Cria o painel das facetas da Atividade Cardíaca (sensor = 4)
function criarPainelAtividadeCardiaca(facetas, facetasTipo) {
    var row = document.getElementById("mainRow");
    if (!document.getElementById("painelFacetas")) {
        var divPainel = document.createElement("div");
        divPainel.setAttribute("id", "painelFacetas");
        preencheCheckboxsFacetas(facetas, divPainel, facetasTipo, "Atividade_cardiaca");
        row.appendChild(divPainel);
        pesquisaResultadosAJAX("Atividade_cardiaca", facetas);
    } else {
        row.childNodes[1].textContent = "";
        var divPainel = document.getElementById("painelFacetas");
        preencheCheckboxsFacetas(facetas, divPainel, facetasTipo, "Atividade_cardiaca");
        row.appendChild(divPainel);
        pesquisaResultadosAJAX("Atividade_cardiaca", facetas);
    }
}

// Preenche as checkboxs do painel de facetas
function preencheCheckboxsFacetas(facetas, divPainel, tipo, sensor) {
    facetasResultados = facetas;
    var facetasColuna = document.createElement("td");
    facetasColuna.setAttribute("id", "colFacetas");
    var tituloPainel = document.createElement("h4");
    tituloPainel.appendChild(document.createTextNode("Pesquisa por Facetas"));
    facetasColuna.appendChild(tituloPainel);

    for (var i = 0; i < facetas.length; i++) {
        if (tipo[i] != "hora" && !facetas[i].includes("GPS") && !facetas[i].includes("Foto")) {
            var checkbox = document.createElement("input");
            checkbox.setAttribute("type", "checkbox");
            checkbox.setAttribute("value", tipo[i]);
            checkbox.setAttribute("id", facetas[i]);
            checkbox.setAttribute("class", sensor);
            checkbox.setAttribute("onclick", "mostraValoresFaceta()");
            var divValores = document.createElement("div");
            divValores.setAttribute("id", "divValores" + facetas[i]);
            divValores.setAttribute("style", "display:none;")
            facetasColuna.appendChild(checkbox);
            facetasColuna.appendChild(document.createTextNode(facetas[i]));
            facetasColuna.appendChild(divValores);
            facetasColuna.appendChild(document.createElement("br"));
        }
    }
    var button = document.createElement("input");
    button.setAttribute("type", "button");
    button.setAttribute("onclick", "atualizaResultadosAJAX()");
    button.setAttribute("id", "pesquisaResultados");
    button.setAttribute("value", "Atualiza Resultados");
    button.setAttribute("class", sensor);
    divPainel.appendChild(facetasColuna);
    divPainel.appendChild(button);
}

// Mostra o valor da faceta selecionada
function mostraValoresFaceta() {
    var checkbox = document.getElementById(event.target.id);
    var divValores = document.getElementById("divValores" + event.target.id);
    if (checkbox.checked) {
        divValores.setAttribute("style", "display:block;");

        switch (checkbox.getAttribute("value")) {
            case "data":
                divValores.appendChild(document.createElement("br"));
                var ul = document.createElement("ul");
                ul.setAttribute("class", "divValoresUl");

                var li1 = document.createElement("li");
                li1.appendChild(document.createTextNode("De: "));
                var dataInicio = document.createElement("input");
                dataInicio.setAttribute("id", "dataInicio_" + event.target.id);
                dataInicio.setAttribute("class", checkbox.getAttribute("class"));
                dataInicio.setAttribute("type", "date");
                dataInicio.setAttribute("value", "2015-01-01");
                li1.appendChild(dataInicio);
                ul.appendChild(li1);

                var li2 = document.createElement("li");
                li2.appendChild(document.createTextNode("Até: "));
                var dataFim = document.createElement("input");
                dataFim.setAttribute("id", "dataFim_" + event.target.id);
                dataFim.setAttribute("class", checkbox.getAttribute("class"));
                dataFim.setAttribute("type", "date");
                dataFim.setAttribute("value", "2016-12-31");
                li2.appendChild(dataFim);
                ul.appendChild(li2);
                divValores.appendChild(ul);

                break;

            case "alfanumérico":
                divValores.appendChild(document.createElement("br"));
                pedeValoresAlfanuméricosFacetasAJAX(event.target.id, checkbox.getAttribute("class"));
                break;

            case "numérico":
                var ul = document.createElement("ul");
                ul.setAttribute("class", "divValoresUl");

                ul.appendChild(document.createElement("br"));

                var li1 = document.createElement("li");
                li1.appendChild(document.createTextNode("De: "));
                var text1 = document.createElement("input");
                text1.setAttribute("type", "text");
                text1.setAttribute("id", "text1" + event.target.id);
                text1.setAttribute("class", checkbox.getAttribute("class"));
                text1.setAttribute("value", "0");
                li1.appendChild(text1);
                ul.appendChild(li1);


                ul.appendChild(document.createElement("br"));

                var li2 = document.createElement("li");
                li2.appendChild(document.createTextNode("Até: "));
                var text2 = document.createElement("input");
                text2.setAttribute("type", "text");
                text2.setAttribute("id", "text2" + event.target.id);
                text2.setAttribute("class", checkbox.getAttribute("class"));
                text2.setAttribute("value", "0");
                li2.appendChild(text2);
                ul.appendChild(li2);

                ul.appendChild(document.createElement("br"));

                var checkbox1 = document.createElement("input");
                checkbox1.setAttribute("type", "checkbox");
                checkbox1.setAttribute("id", "semValor" + event.target.id);
                checkbox1.setAttribute("class", checkbox.getAttribute("class"));
                ul.appendChild(checkbox1);
                ul.appendChild(document.createTextNode("Sem Valor Atribuído"));

                divValores.appendChild(ul);
                break;
        }

    } else {
        divValores.setAttribute("style", "display:none;");
        divValores.textContent = "";

    }
}

// Faz o pedido AJAX ao servidor para os valores alfanuméricos das facetas
function pedeValoresAlfanuméricosFacetasAJAX(faceta, sensor) {
    url = "http://phpdev2.dei.isep.ipp.pt/~arqsi/smartcity/valoresFacetadoSensor.php?sensor=" + sensor + "&faceta=" + faceta;
    makeXmlHttpGetCall(url, null, true, false, pedeValoresAlfanuméricosFacetasJSON, [sensor, faceta]);
}

// Trata da resposta do servidor em JSON para os valores alfanuméricos
function pedeValoresAlfanuméricosFacetasJSON(objJSON, args) {
    var valoresFaceta = JSON.parse(objJSON);
    var divValores = document.getElementById("divValores" + args[1]);
    var ul = document.createElement("ul");
    ul.setAttribute("class", "divValoresUl");

    for (i = 0; i < valoresFaceta.length; i++) {
        var li = document.createElement("li");
        var checkbox = document.createElement("input");
        checkbox.setAttribute("type", "checkbox");
        checkbox.setAttribute("id", "valor_" + args[1] + "_" + valoresFaceta[i]);
        checkbox.setAttribute("class", args[0]);
        checkbox.setAttribute("value", valoresFaceta[i]);
        checkbox.setAttribute("name", valoresFaceta[i]);
        li.appendChild(checkbox);
        li.appendChild(document.createTextNode(valoresFaceta[i]));
        ul.appendChild(li);
    }
    divValores.appendChild(ul);
}

// Faz o pedido AJAX para a pesquisa global de resultados
function pesquisaResultadosAJAX(sensor, facetas) {
    url = "http://phpdev2.dei.isep.ipp.pt/~arqsi/smartcity/valoresDeSensor.php?sensor=" + sensor;
    makeXmlHttpGetCall(url, null, true, false, pesquisaResultadosJSON, facetas);

}

// Trata dos resultados recebidos em JSON
function pesquisaResultadosJSON(objJSON, facetas) {
    var resultados = JSON.parse(objJSON);
    preencheResultados(resultados, facetas);
}

// Preenche a tabela de resultados
function preencheResultados(resultados, facetas) {
    var form = document.getElementById("widgetForm");

    if (!document.getElementById("divResultados")) {
        var divResultados = document.createElement("div");
        divResultados.setAttribute("id", "divResultados");
    } else {
        var divResultados = document.getElementById("divResultados");
        divResultados.textContent = "";
    }

    divResultados.appendChild(document.createElement("br"));
    var tableResultados = document.createElement("table");
    tableResultados.setAttribute("id", "tabelaResultados");
    var tr1Tabela = document.createElement("tr");

    for (i = 0; i < facetas.length; i++) {
        var th = document.createElement("th");
        th.appendChild(document.createTextNode(" " + facetas[i] + " "));
        tr1Tabela.appendChild(th);
    }

    tableResultados.appendChild(tr1Tabela);

    for (i = 0; i < resultados.length; i++) {
        var row = document.createElement("tr");
        for (j = 0; j < facetas.length; j++) {
            var col = document.createElement("td");
            var obj = resultados[i];
            var valor = document.createTextNode(obj[facetas[j]]);
            if (obj[facetas[j]].includes("http")) {
                var link = document.createElement("a");
                link.setAttribute("href", obj[facetas[j]]);
                var icon = document.createElement("img");
                icon.setAttribute("src", "http://www.nuncadesnorteados.com/wp-content/themes/nuncadesnorteados/images/social-network_instagram-icon.png");
                link.appendChild(icon);
                col.appendChild(link);

            } else {
                col.appendChild(valor);
            }

            row.appendChild(col);
        }
        tableResultados.appendChild(row);
    }

    divResultados.appendChild(tableResultados);
    form.appendChild(divResultados);
}

// Atualiza os resultados após a selecção dos valores das facetas (AJAX)
function atualizaResultadosAJAX() {
    var painelFacetas = document.getElementById("colFacetas");
    var sensor = (event.target).getAttribute("class");
    var checkboxs = painelFacetas.getElementsByClassName(sensor);
    var facetasValores = [];
    var x = 0;

    for (i = 0; i < checkboxs.length; i++) {
        if (checkboxs[i].checked && checkboxs[i].getAttribute("name") != null) {

            var aux1 = checkboxs[i].getAttribute("id");
            var aux2 = aux1.split("_");
            var aux3 = [];
            aux3[0] = aux2[1];
            aux3[1] = aux2[2];
            facetasValores[x] = aux3;
            x++;
        }
    }
    var url = "http://phpdev2.dei.isep.ipp.pt/~arqsi/smartcity/valoresDeSensor.php?sensor=" + sensor;

    for (j = 0; j < facetasValores.length; j++) {
        var aux = facetasValores[j];
        if (!url.includes(aux[0])) {
            url += "&" + aux[0] + "=";
            var valores = [];
            for (k = 0; k < facetasValores.length; k++) {
                var aux1 = facetasValores[k];
                if (aux[0] == aux1[0]) {
                    valores.push(aux1[1]);
                }
            }
            url += valores;
        }
    }

    makeXmlHttpGetCall(url, null, true, false, atualizaResultadosJSON, [facetasResultados, sensor]);
}

// Atualiza os resultados após a selecção dos valores das facetas (JSON)
function atualizaResultadosJSON(objJSON, valores) {
    var resultados = JSON.parse(objJSON);
    var painelFacetas = document.getElementById("colFacetas");
    var checkboxs = painelFacetas.getElementsByClassName(valores[1]);

    // Verifica datas
    var resultDatas = [];
    for (i = 0; i < checkboxs.length; i++) {
        if (checkboxs[i].checked && checkboxs[i].getAttribute("value") == "data") {
            var aux1 = document.getElementById("dataInicio_" + checkboxs[i].getAttribute("id"));
            var dataInicio = new Date(aux1.value);
            var aux2 = document.getElementById("dataFim_" + checkboxs[i].getAttribute("id"));
            var dataFim = new Date(aux2.value);

            for (j = 0; j < resultados.length; j++) {
                var aux3 = resultados[j].Data_de_leitura;
                var data = new Date(aux3);
                if (data >= dataInicio && data <= dataFim) {
                    resultDatas.push(resultado[j]);
                }
            }
        }
    }

    if(resultDatas.length == 0) {
        resultDatas = resultados;
    }

    // Valores Númericos
    var resultNum = [];
    for (i = 0; i < checkboxs.length; i++) {
        if (checkboxs[i].checked && checkboxs[i].getAttribute("value") == "numérico") {
            var valor1 = document.getElementById("text1" + checkboxs[i].getAttribute("id"));
            valor1 = (valor1.value).replace(",", ".");
            var valor2 = document.getElementById("text2" + checkboxs[i].getAttribute("id"));
            valor2 = (valor2.value).replace(",", ".");
            var regex=/^[0-9]+$/;
            if (valor1.match(regex) && valor2.match(regex)) {
                var checkbox = document.getElementById("semValor" + checkboxs[i].getAttribute("id"));
                var faceta = checkboxs[i].getAttribute("id");

                for (j = 0; j < resultDatas.length; j++) {
                    var obj = resultDatas[j];
                    var valor3 = obj[faceta];
                    if (valor3 == "" && checkbox.checked) {
                        resultNum.push(resultDatas[j]);
                    }

                    valor3 = valor3.replace(",", ".");
                    if (valor3 >= valor1 && valor3 <= valor2 && valor1 != 0 && valor2 != 0) {
                        resultNum.push(resultDatas[j]);
                    }
                }
            }
        }
    }

    if(resultNum.length == 0) {
        resultNum = resultados;
    }


    preencheResultados(resultNum, valores[0]);
}

function pesquisarMeteorologiasAJAX()
{
    url = "http://localhost:24555/api/Meteorologias";
    makeXmlHttpGetCall(url, null, true, false, pesquisarMeteorologiasJSON, null);

}
