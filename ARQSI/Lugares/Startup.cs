﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin;
using ModelARQSI.Models;
using Owin;

[assembly: OwinStartupAttribute(typeof(Lugares.Startup))]
namespace Lugares
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            createRolesandUsers();
        }


        // In this method we will create default User roles and Admin user for login   
        private void createRolesandUsers()
        {
            ApplicationDbContext context = new ApplicationDbContext();

            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));
            var UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));


            // In Startup iam creating first Editor Role and creating a default Editor User    
            if (!roleManager.RoleExists("Editor"))
            {

                // first we create Editor rool   
                var role = new Microsoft.AspNet.Identity.EntityFramework.IdentityRole();
                role.Name = "Editor";
                roleManager.Create(role);

                var user = UserManager.FindByEmail("123@email.com");
                var result1 = UserManager.AddToRole(user.Id, "Editor");
            }

        }
    }
}
