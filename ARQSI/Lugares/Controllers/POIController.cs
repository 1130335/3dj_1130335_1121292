﻿using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using ModelARQSI.DAL;
using ModelARQSI.Model;

namespace Lugares.Controllers
{
    [Authorize(Roles = "Editor")]
    public class POIController : Controller
    {
        private DatumContext db = new DatumContext();

        // GET: POI
        public ActionResult Index()
        {
            var pOIs = db.POIs.Include(p => p.Local);
            return View(pOIs.ToList());
        }

        // GET: POI/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            POI pOI = db.POIs.Find(id);
            if (pOI == null)
            {
                return HttpNotFound();
            }
            return View(pOI);
        }

        // GET: POI/Create
        public ActionResult Create()
        {
            ViewBag.LocalID = new SelectList(db.Locals, "ID", "Nome");
            return View();
        }

        // POST: POI/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,LocalID,Nome,Descricao")] POI pOI)
        {
            if (ModelState.IsValid)
            {
                pOI.Criador = User.Identity.Name;
                db.POIs.Add(pOI);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.LocalID = new SelectList(db.Locals, "ID", "Nome", pOI.LocalID);
            return View(pOI);
        }

        // GET: POI/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            POI pOI = db.POIs.Find(id);
            if (pOI == null)
            {
                return HttpNotFound();
            }
            ViewBag.LocalID = new SelectList(db.Locals, "ID", "Nome", pOI.LocalID);
            return View(pOI);
        }

        // POST: POI/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,LocalID,Nome,Descricao")] POI pOI)
        {
            if (User.Identity.Name == pOI.Criador)
            {
                if (ModelState.IsValid)
                {
                    db.Entry(pOI).State = EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
            }
            ViewBag.LocalID = new SelectList(db.Locals, "ID", "Nome", pOI.LocalID);
            return View(pOI);
        }

        // GET: POI/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            POI pOI = db.POIs.Find(id);
            if (pOI == null)
            {
                return HttpNotFound();
            }
            return View(pOI);
        }

        // POST: POI/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            POI pOI = db.POIs.Find(id);
            if (User.Identity.Name == pOI.Criador) { 
                db.POIs.Remove(pOI);
            db.SaveChanges(); }
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
