﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;

namespace Visita.Helpers
{
    public static class WebApiHttpClient
    {
        public const string WebApiBaseAddress = "http://localhost:24555";

        public static HttpClient GetClient()
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(WebApiBaseAddress);
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            return client;
        }
    }
}