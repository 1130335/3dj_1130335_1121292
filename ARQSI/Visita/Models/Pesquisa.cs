﻿using ModelARQSI.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Visita.Models
{
    public class Pesquisa
    {
        public virtual POI POI { get; set; }
        public int POIID { get; set; }
        [DataType(DataType.Date)]
        public DateTime? PrimeiraData { get; set; }

        [DataType(DataType.Date)]
        public DateTime? SegundaData { get; set; }
    }
}