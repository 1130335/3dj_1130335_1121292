﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ModelARQSI.DAL;
using ModelARQSI.Model;
using Visita.Helpers;
using System.Net.Http;
using Newtonsoft.Json;

namespace Visita.Controllers
{
    public class MeteorologiaController : Controller
    {
        private DatumContext db = new DatumContext();

        // GET: Meteorologia
        public async Task<ActionResult> Index()
        {
            var client = WebApiHttpClient.GetClient();
            HttpResponseMessage response = await client.GetAsync("api/Meteorologias");
            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                var met =
                JsonConvert.DeserializeObject<IEnumerable<Meteorologia>>(content);
                return View(met);
            }
            else
            {
                return Content("Ocorreu um erro: " + response.StatusCode);
            }
        }

        // GET: Meteorologia/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Meteorologia meteorologia = await db.Meteorologias.FindAsync(id);
            if (meteorologia == null)
            {
                return HttpNotFound();
            }
            return View(meteorologia);
        }

        // GET: Meteorologia/Create
        public ActionResult Create()
        {
            ViewBag.LocalID = new SelectList(db.Locals, "ID", "Nome");
            return View();
        }

        // POST: Meteorologia/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "ID,LocalID,DataDeLeitura,HoraDeLeitura,Temp,Vento,Humidade,Pressao,NO,NO2,CO2")] Meteorologia meteorologia)
        {
            if (ModelState.IsValid)
            {
                db.Meteorologias.Add(meteorologia);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.LocalID = new SelectList(db.Locals, "ID", "Nome", meteorologia.LocalID);
            return View(meteorologia);
        }

        // GET: Meteorologia/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Meteorologia meteorologia = await db.Meteorologias.FindAsync(id);
            if (meteorologia == null)
            {
                return HttpNotFound();
            }
            ViewBag.LocalID = new SelectList(db.Locals, "ID", "Nome", meteorologia.LocalID);
            return View(meteorologia);
        }

        // POST: Meteorologia/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "ID,LocalID,DataDeLeitura,HoraDeLeitura,Temp,Vento,Humidade,Pressao,NO,NO2,CO2")] Meteorologia meteorologia)
        {
            if (ModelState.IsValid)
            {
                db.Entry(meteorologia).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.LocalID = new SelectList(db.Locals, "ID", "Nome", meteorologia.LocalID);
            return View(meteorologia);
        }

        // GET: Meteorologia/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Meteorologia meteorologia = await db.Meteorologias.FindAsync(id);
            if (meteorologia == null)
            {
                return HttpNotFound();
            }
            return View(meteorologia);
        }

        // POST: Meteorologia/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Meteorologia meteorologia = await db.Meteorologias.FindAsync(id);
            db.Meteorologias.Remove(meteorologia);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
