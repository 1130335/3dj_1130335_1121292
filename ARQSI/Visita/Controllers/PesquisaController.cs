﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ModelARQSI.DAL;
using Visita.Models;
using Visita.Helpers;
using System.Net.Http;
using Newtonsoft.Json;
using ModelARQSI.Model;
using System.Diagnostics;

namespace Visita.Controllers
{
    public class PesquisaController : Controller
    {
        private List<Meteorologia> mets = new List<Meteorologia>();
        //private DatumContext db = new DatumContext();

        //// GET: Pesquisa
        public async Task<ActionResult> Index()
        {
            foreach (Meteorologia m in mets)
            {
                Debug.WriteLine("Meteorologia:" + m.ID);
                Debug.WriteLine("Meteorologia:" + m.Local.Nome);
            }
            return View(mets);
        }

        //// GET: Pesquisa/Details/5
        //public async Task<ActionResult> Details(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    Pesquisa pesquisa = await db.Pesquisas.FindAsync(id);
        //    if (pesquisa == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(pesquisa);
        //}

        // GET: Pesquisa/Create
        public async Task<ActionResult> Pesquisa()
        {
            var client = WebApiHttpClient.GetClient();
            HttpResponseMessage response = await client.GetAsync("api/POIs");
            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                var POIs =
                JsonConvert.DeserializeObject<IEnumerable<POI>>(content);
                ViewBag.POIID = new SelectList(POIs, "ID", "Nome");
                return View();
            }
            else
            {
                return Content("Ocorreu um erro: " + response.StatusCode);
            }
        }

        // POST: Pesquisa/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Pesquisa([Bind(Include = "POIID,PrimeiraData,SegundaData")] Pesquisa pesquisa)
        {
            var client = WebApiHttpClient.GetClient();
            IEnumerable<POI> POIs;
            IEnumerable<Meteorologia> Meteorologias;
            HttpResponseMessage response = await client.GetAsync("api/POIs");
            if (response.IsSuccessStatusCode)
            {

                string content = await response.Content.ReadAsStringAsync();
                POIs =
                JsonConvert.DeserializeObject<IEnumerable<POI>>(content);

            }
            else
            {
                return Content("Ocorreu um erro: " + response.StatusCode);
            }

            HttpResponseMessage response1 = await client.GetAsync("api/POIs");
            if (response.IsSuccessStatusCode)
            {

                string content1 = await response.Content.ReadAsStringAsync();
                Meteorologias =
                JsonConvert.DeserializeObject<IEnumerable<Meteorologia>>(content1);

            }
            else
            {
                return Content("Ocorreu um erro: " + response.StatusCode);
            }

            foreach (POI p in POIs)
            {
                if (p.ID == pesquisa.POIID)
                {
                    int localID = p.LocalID;


                    foreach (Meteorologia m in Meteorologias)
                    {
                        //DateTime date;
                        //Debug.WriteLine("DATAAAAAA: " + m.DataDeLeitura);
                        //if (DateTime.TryParse(m.DataDeLeitura, out date) == true)
                        //{
                        //    Debug.WriteLine(date.ToString());
                        //}


                        if (m.LocalID == localID)
                        {
                            //Debug.WriteLine("Meteorologia:" + m.ID);
                            mets.Add(m);
                            //int prim = Nullable.Compare<DateTime>(date, pesquisa.PrimeiraData);
                            //Debug.WriteLine("Primeira data:" + prim);
                            //int seg = Nullable.Compare<DateTime>(date, pesquisa.SegundaData);
                            //Debug.WriteLine("Segunda data:" + seg);
                            //if (prim >= 0 && seg <= 0)
                            //{
                            //    mets.Add(m);
                            //    Debug.WriteLine("Meteorologia:" + m.ID);
                            //}
                        }
                    }
                }


            }

            return RedirectToAction("Index");

        }

        //// GET: Pesquisa/Edit/5
        //public async Task<ActionResult> Edit(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    Pesquisa pesquisa = await db.Pesquisas.FindAsync(id);
        //    if (pesquisa == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(pesquisa);
        //}

        //// POST: Pesquisa/Edit/5
        //// To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        //// more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> Edit([Bind(Include = "ID,PrimeiraData,SegundaData")] Pesquisa pesquisa)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        db.Entry(pesquisa).State = EntityState.Modified;
        //        await db.SaveChangesAsync();
        //        return RedirectToAction("Index");
        //    }
        //    return View(pesquisa);
        //}

        //// GET: Pesquisa/Delete/5
        //public async Task<ActionResult> Delete(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    Pesquisa pesquisa = await db.Pesquisas.FindAsync(id);
        //    if (pesquisa == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(pesquisa);
        //}

        //// POST: Pesquisa/Delete/5
        //[HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> DeleteConfirmed(int id)
        //{
        //    Pesquisa pesquisa = await db.Pesquisas.FindAsync(id);
        //    db.Pesquisas.Remove(pesquisa);
        //    await db.SaveChangesAsync();
        //    return RedirectToAction("Index");
        ////}

        //protected override void Dispose(bool disposing)
        //{
        //    if (disposing)
        //    {
        //        db.Dispose();
        //    }
        //    base.Dispose(disposing);
        //}
    }
}
