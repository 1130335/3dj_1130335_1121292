namespace ModelARQSI.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialMigration : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Local",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        GPS_Lat = c.Double(nullable: false),
                        GPS_Long = c.Double(nullable: false),
                        Nome = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Meteorologia",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        LocalID = c.Int(nullable: false),
                        DataDeLeitura = c.DateTime(),
                        HoraDeLeitura = c.String(),
                        Temp = c.Int(nullable: false),
                        Vento = c.Int(nullable: false),
                        Humidade = c.Int(nullable: false),
                        Pressao = c.Double(nullable: false),
                        NO = c.String(),
                        NO2 = c.String(),
                        CO2 = c.String(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Local", t => t.LocalID, cascadeDelete: true)
                .Index(t => t.LocalID);
            
            CreateTable(
                "dbo.POI",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        LocalID = c.Int(nullable: false),
                        Nome = c.String(nullable: false),
                        Descricao = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Local", t => t.LocalID, cascadeDelete: true)
                .Index(t => t.LocalID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.POI", "LocalID", "dbo.Local");
            DropForeignKey("dbo.Meteorologia", "LocalID", "dbo.Local");
            DropIndex("dbo.POI", new[] { "LocalID" });
            DropIndex("dbo.Meteorologia", new[] { "LocalID" });
            DropTable("dbo.POI");
            DropTable("dbo.Meteorologia");
            DropTable("dbo.Local");
        }
    }
}
