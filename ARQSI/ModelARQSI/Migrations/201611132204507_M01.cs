namespace ModelARQSI.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class M01 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Meteorologia", "DataDeLeitura", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Meteorologia", "DataDeLeitura", c => c.DateTime());
        }
    }
}
