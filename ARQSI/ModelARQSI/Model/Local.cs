﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelARQSI.Model
{
    public class Local
    {
        public int ID { get; set; }

        [DisplayName("GPS: Latitude")]
        public double GPS_Lat { get; set; }

        [DisplayName("GPS: Longitude")]
        public double GPS_Long { get; set; }

        [DisplayName("Nome do Local")]
        public string Nome { get; set; }
    }
}
