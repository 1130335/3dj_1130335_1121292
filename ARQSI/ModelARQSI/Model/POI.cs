﻿using ModelARQSI.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelARQSI.Model
{
    public class POI
    {
        public int ID { get; set; }
        public int LocalID { get; set; }

        [DisplayName("Nome do POI")]
        [Required]
        public string Nome { get; set; }

        [DisplayName("Descrição do POI")]
        [Required]
        public string Descricao { get; set; }

        [DisplayName("Criador do POI")]
        public string Criador { get; set; }

        public virtual Local Local { get; set; }
    }
}
