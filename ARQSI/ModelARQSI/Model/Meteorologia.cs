﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelARQSI.Model
{
   public class Meteorologia
    {
        public int ID { get; set; }
        public int LocalID { get; set; }
        public string DataDeLeitura { get; set; }
        public string HoraDeLeitura { get; set; }
        public int Temp { get; set; }
        public int Vento { get; set; }
        public int Humidade { get; set; }
        public double Pressao { get; set; }
        public string NO { get; set; }
        public string NO2 { get; set; }
        public string CO2 { get; set; }
        public virtual Local Local { get; set; }
    }
}
