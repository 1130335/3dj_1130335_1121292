﻿using ModelARQSI.Model;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace ModelARQSI.DAL
{
    public class DatumContext : DbContext 
    {
        public DatumContext() : base("Datum")
        {
        }

        public DbSet<Local> Locals { get; set; }
        public DbSet<POI> POIs { get; set; }
        public DbSet<Meteorologia> Meteorologias { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }

       

        
    }
}