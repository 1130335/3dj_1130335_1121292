﻿using ModelARQSI.DAL;
using ModelARQSI.Model;
using System;
using System.Collections.Generic;

namespace ModelARQSI.DAL
{
    public class DatumInitializer : System.Data.Entity.DropCreateDatabaseIfModelChanges<DatumContext>
    {
        protected override void Seed(DatumContext context)
        {
            //var locals = new List<Local>
            //{
            //    new Local
            //    {
            //        Nome="Porto",
            //        GPS_Lat=0.2402821479,
            //        GPS_Long=0.3939282440
            //    },

            //    new Local
            //    {
            //        Nome="Vila Nova de Gaia",
            //        GPS_Lat=0.0523578701,
            //        GPS_Long=0.7532860512
            //    },

            //    new Local
            //    {
            //        Nome="Ermesinde",
            //        GPS_Lat=0.3310635945,
            //        GPS_Long=0.4878486362
            //    },
            //};
            //locals.ForEach(s => context.Locals.Add(s));
            //context.SaveChanges();

            //var pois = new List<POI>
            //{
            //    Dados teste a introduzir
            //};
            //pois.ForEach(s => context.POIs.Add(s));
            //context.SaveChanges();

            //var meteos = new List<Meteorologia>
            //    {
            //        new Meteorologia
            //        {
            //            LocalID = 1,
            //            DataDeLeitura = "12/10/2016",
            //            HoraDeLeitura = "14:30",
            //            Temp = 22,
            //            Vento = 3,
            //            Humidade = 30,
            //            Pressao = 1024,
            //            NO = "7",
            //            NO2 = "10",
            //            CO2 = "300",
            //            Local = context.Locals.Find(1),
            //        },

            //        new Meteorologia
            //        {
            //            LocalID = 1,
            //            DataDeLeitura = "27/10/2016",
            //            HoraDeLeitura = "18:51",
            //            Temp = 17,
            //            Vento = 1,
            //            Humidade = 45,
            //            Pressao = 1072,
            //            NO = "20",
            //            NO2 = "30",
            //            CO2 = "400",
            //            Local = context.Locals.Find(1),
            //        },

            //        new Meteorologia
            //        {
            //            LocalID = 3,
            //            DataDeLeitura = "2/11/2016",
            //            HoraDeLeitura = "10:45",
            //            Temp = 12,
            //            Vento = 4,
            //            Humidade = 60,
            //            Pressao = 1063,
            //            NO = "5",
            //            NO2 = "10",
            //            CO2 = "250",
            //            Local = context.Locals.Find(3),
            //          },
            //    };
            //meteos.ForEach(s => context.Meteorologias.Add(s));
            //context.SaveChanges();

        }
    }
}