<?php
namespace Percurso\Model;

class Percurso
{
    public $percursoID;
    public $turistaID;
    public $descricao;
    public $dataPercurso;
    
    public function exchangeArray($data) {
        $this->percursoID = (!empty($data['percursoID'])) ? $data['percursoID'] : null;
        $this->turistaID = (!empty($data['turistaID'])) ? $data['turistaID'] : null;
        $this->descricao = (!empty($data['descricao'])) ? $data['descricao'] : null;
        $this->dataPercurso = (!empty($data['dataPercurso'])) ? $data['dataPercurso'] : null;
        
    }
    
    
}

