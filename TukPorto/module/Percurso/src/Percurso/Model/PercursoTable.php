<?php
namespace Percurso\Model;

use Zend\Db\TableGateway\TableGateway;
use Percurso\Model\Percurso;

class PercursoTable
{
    protected $tableGateway;
    
    public function __construct(TableGateway $tableGateway) {
        $this->tableGateway = $tableGateway;
    }
    
    public function fetchAll() {
        $resultSet = $this->tableGateway->select();
        return $resultSet;
    }
    
    public function getPercurso($id) {
        $id = (int) $id;
        $rowSet = $this->tableGateway->select(array('PercursoID' => $id));
        $row->$rowSet->current();
        if(!$row) {
            throw new \Exception("N�o foi poss�vel encontrar a linha $id");
        }
        return $row;
    }
    
    public function savePercurso(Percurso $percurso) {
        $data = array (
            'TuristaID' => $percurso->turistaID,
            'Descricao' => $percurso->descricao,
            'Data' => $percurso->dataPercurso,
        );
        
        $id = (int) $percurso->id;
        if($id == 0) {
            $this->tableGateway->insert($data);
        } else {
            if($this->getPercurso($id)) {
                $this->tableGateway->update($data, array('PercursoID' => $id));
            } else {
               throw new \Exception("Percurso $id n�o existe");
            }
        }
    }
    
    public function deletePercurso($id) {
        $this->tableGateway->delete(array('PercursoID' => (int) $id));
    }
}

