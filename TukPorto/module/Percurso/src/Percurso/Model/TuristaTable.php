<?php
namespace Percurso\Model;

use Zend\Db\TableGateway\TableGateway;
use Percurso\Model\Turista;

class TuristaTable
{
    protected $tableGateway;
    
    public function __construct(TableGateway $tableGateway) {
        $this->tableGateway = $tableGateway;
    }
    
    public function fetchAll() {
        $resultSet = $this->tableGateway->select();
        return $resultSet;
    }
    
    public function getTurista($id) {
        $id = (int) $id;
        $rowSet = $this->tableGateway->select(array('TuristaID' => $id));
        $row->$rowSet->current();
        if(!$row) {
            throw new \Exception("N�o foi poss�vel encontrar a linha $id");
        }
        return $row;
    }
    
    public function saveTurista(Turista $turista) {
        $data = array (
            'Nome' => $turista->nome,
            'Password' => $turista->password,
            'Nacionalidade' => $turista->nacionalidade,
            'Email' => $turista->email,
        );
    
        $id = (int) $turista->id;
        if($id == 0) {
            $this->tableGateway->insert($data);
        } else {
            if($this->getTurista($id)) {
                $this->tableGateway->update($data, array('TuristaID' => $id));
            } else {
                throw new \Exception("Turista $id n�o existe");
            }
        }
    }
    
    public function deleteTurista($id) {
        $this->tableGateway->delete(array('TuristaID' => (int) $id));
    }
}

