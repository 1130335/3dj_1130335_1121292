<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/Percurso for the canonical source repository
 * @copyright Copyright (c) 2005-2015 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Percurso\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Percurso\Model\Turista;
use Percurso\Form\RegisterForm;
use Percurso\Form\LoginForm;

class PercursoController extends AbstractActionController
{
    public function indexAction()
    {
        return array();
    }

    public function addAction()
    {
        return array();
    }
    
    public function editAction()
    {
        return array();
    }
    
    public function deleteAction()
    {
        return array();
    }
    
    public function homeAction() {
        $storage = new Zend_Auth_Storage_Session();
        $data = $storage->read();
        if(!$data) {
            $this->_redirect('percurso/login');
        }
    }
    
    public function registerAction()
    {
        $form = new RegisterForm();
        $form->get('submit')->setValue('Criar Conta');
         
        $request = $this->getRequest();
        if($request->isPost()) {
            $turista = new Turista();
            $form->setInputFilter($turista->getInputFilter());
            $form->setData($request->getPost());
             
            if($form->isValid()) {
                $turista->exchangeArray($form->getData());
                $this->getTuristaTable()->saveTurista($turista);
                 
                $this->redirect('percurso/login');
            }
        }
    }
    
    public function loginAction() {
        $form = new LoginForm();
        $form->get('submit')->setValue('Login');
    
        $request = $this->getRequest();
        if($request->isPost()) {
            $email = $request->getPost('email');
            $password = $request->getPost('password');
    
            if(empty($email) || empty($password)) {
                $this->view->errors[] = "Insira o email e a password";
            } else {
                $db = Zend_Db_Table::getDefaultAdapter();
                $authAdapter = new Zend_Auth_Adapter_DbTable($db);
    
                $authAdapter->setTableName('conta');
                $authAdapter->setIdentityColumn('email');
                $authAdapter->setCredentialColumn('password');
                $authAdapter->setCredentialTreatment('MD5 (?)');
    
                $authAdapter->setIdentity($email);
                $authAdapter->setCredential($password);
    
                $auth = Zend_Auth::getInstance();
                $result = $auth->authenticate($authAdapter);
    
                if($result->isValid()) {
                    $this->_redirect('/');
                } else {
                    $this->view->errors[] = "Login falhou. Verifique os dados de acesso.";
                }
            }
        }
    }
    
    public function logoutAction() {
        $storage = new Zend_Auth_Storage_Session();
        $storage->clear();
        $this->_redirect('percurso/login');
    }
}
