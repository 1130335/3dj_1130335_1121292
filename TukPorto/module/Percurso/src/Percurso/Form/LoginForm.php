<?php
namespace Percurso\Form;
use Zend\Form\Form;
use Zend\Form\Element;
use Zend\Form\Fieldset;

class LoginForm extends Form
{
    public function __construct($name=null) {
        parent::__construct('login');
        
        $this->add(array(
            'name' => 'email',
            'type' => 'Text',
            'options' => array(
                'label' => 'Email'
            ),
        ));
        $this->add(array(
            'name' => 'password',
            'type' => 'Text',
            'options' => array(
                'label' => 'Password'
            ),
        ));

        $this->add(array(
            'name' => 'submit',
            'type' => 'Submit',
            'attributes' => array(
                'value' => 'Go',
                'id' => 'submitbutton',
            ),
        ));
    }
    
}

